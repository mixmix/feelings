# Feelings

I was pair programming with a friend:
> **Mix**: I'm having some strong feelings about this library. I think it might be good to name them cos they're getting in the way of me being able to calmly engage with this.
> **Ben**: Bro have you seen the FeelingsWheel?

https://feelingswheel.com/

It's great! But then I noticed it was a rasterized image... and I had some more feelings. Shocked? Provoked? Well here I am making a vector version in raw JS + HTML

You can find this online at https://mixmix.gitlab.io/feelings

## Features

- [x] draw segments as SVG
- [x] make segments interactive on:hover
- [x] labels for segments
- [x] data model for feelings hierarchy (names, colors)
- [ ] map data model => diagram
- [ ] feelings search
- [ ] hide parts of the wheel you're not interested in?
- [ ] highlight multiple feelings (requires state)

## Setup

If you want to hack on this locally, install Node, and then run

```bash
npx serve src
```

