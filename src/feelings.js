// { _: true, _: true } = you have reached the bottom

export default {
  Fearful: {
    Scared: {
      Helpless: true,
      Frightened: true,
    },
    Anxious: {
      Overwhelmed: true,
      Worried: true,
    },
    Insecure: {
      Inadequate: true,
      Inferior: true,
    },
    Weak: {
      Worthless: true,
      Insignificant: true,
    },
    Rejected: {
      Excluded: true,
      Persecuted: true,
    },
    Threatened: {
      Nervous: true,
      Exposed: true,
    },
  },
  Angry: {
    "Let Down": {
      Betrayed: true,
      Resentful: true,
    },
    Humiliated: {
      Disrespected: true,
      Ridiculed: true,
    },
    Bitter: {
      Indignant: true,
      Violated: true,
    },
    Mad: {
      Furious: true,
      Jealous: true,
    },
    Aggressive: {
      Provoked: true,
      Hostile: true,
    },
    Frustrated: {
      Infuriated: true,
      Annoyed: true,
    },
    Distant: {
      Withdrawn: true,
      Numb: true,
    },
    Critical: {
      Skeptical: true,
      Dismissive: true,
    },
  },
  Disgusted: {
    Disapproving: {
      Judgemental: true,
      Embarassed: true,
    },
    Disappointed: {
      Appalled: true,
      Revolted: true,
    },
    Awful: {
      Nauseated: true,
      Detestable: true,
    },
    Repelled: {
      Horrified: true,
      Hesitant: true,
    },
  },
  Sad: {
    Hurt: {
      Embarassed: true,
      Disappointed: true,
    },
    Depressed: {
      Inferior: true,
      Empty: true,
    },
    Guilty: {
      Remorseful: true,
      Ashamed: true,
    },
    Despair: {
      Powerless: true,
      Grief: true,
    },
    Vulnerable: {
      Fragile: true,
      Victimized: true,
    },
    Lonely: {
      Abandoned: true,
      Isolated: true,
    },
  },
  Happy: {
    Optimistic: {
      Inspired: true,
      Hopeful: true,
    },
    Trusting: {
      Intimate: true,
      Sensistive: true,
    },
    Peaceful: {
      Thankful: true,
      Loving: true,
    },
    Powerful: {
      Creative: true,
      Courageous: true,
    },
    Accepted: {
      Valued: true,
      Respected: true,
    },
    Proud: {
      Confident: true,
      Successful: true,
    },
    Interested: {
      Inquisitive: true,
      Curious: true,
    },
    Content: {
      Joyful: true,
      Free: true,
    },
    Playful: {
      Cheeky: true,
      Aroused: true,
    },
  },
  Surprised: {
    Excited: {
      Energetic: true,
      Eager: true,
    },
    Amazed: {
      Awe: true,
      Astonished: true,
    },
    Confused: {
      Perplexed: true,
      Disillusioned: true,
    },
    Startled: {
      Dismayed: true,
      Shocked: true,
    },
  },
  Bad: {
    Tired: {
      Unfocused: true,
      Sleepy: true,
    },
    Stressed: {
      "Out of Control": true,
      Stressed: true,
    },
    Busy: {
      Rushed: true,
      Pressured: true,
    },
    Bored: {
      Apathetic: true,
      Indifferent: true,
    },
  },
};
