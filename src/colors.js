export default {
  Fearful: '#d1278a',
  Angry: '#ce2b3c',
  Disgusted: '#774227',
  Sad: '#2e556c',
  Happy: '#e87800',
  Surprised: '#4a7460',
  Bad: '#432362'
}
