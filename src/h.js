export default fixSignature(
  createElement,
  (a, b, c) => ({
    string:               [a, {}, []],
    string_object:        [a, b, []],
    string_array:         [a, {}, b],
    string_string:        [a, {}, b],
    string_object_array:  [a, b, c],
    string_object_string: [a, b, c],
  })
)

function fixSignature (fn, recipe) {
  return function (...args) {
    const signature = getCurrentSignature(args)
    const newArgs = recipe(...args)[signature]
    if (!newArgs) {
      console.error({ signature, args })
      throw new Error(`unknown signature "${signature}"`)
    }

    return fn(...newArgs)
  }

  function getCurrentSignature (args) {
    const signatureArr = args.map(getType)

    // prune trailing undefined's
    while (signatureArr.at(-1) === 'undefined') {
      signatureArr.pop()
    }

    return signatureArr.join('_')
  }
}

function getType (thing) {
  if (thing === undefined) return 'undefined'
  else if (thing === null) return 'null'
  else if (Array.isArray(thing)) return 'array'
  return typeof thing
}

function createElement (tag, attrs, children) {
  return isSVGTag(tag)
    ? createElementSVG(tag, attrs, children)
    : createElementHTML(tag, attrs, children)
}

const SVG_TAGS = new Set([
  'svg',
  'rect',
  'circle',
  'path',
  'text',
  'g'
])
const SVG_NS = 'http://www.w3.org/2000/svg'

function isSVGTag (tag) {
  return SVG_TAGS.has(tag)
}

function createElementSVG (tag, attrs, children) {
  const el = document.createElementNS(SVG_NS, tag)

  for (let a in attrs) {
    // el.setAttributeNS(SVG_NS, a, attrs[a])
    el.setAttribute(a, attrs[a])
  }

  if (Array.isArray(children)) {
    for (let child of children) {
      el.appendChild(child)
    }
  }
  else {
    // console.error('svg child?', child)
    el.innerHTML = children
  }

  return el
}

function createElementHTML (tag, attrs, children) {
  const el = document.createElement(tag)

  for (let a in attrs) {
    el.setAttribute(a, attrs[a])
  }

  if (Array.isArray(children)) {
    for (let child of children) {
      el.appendChild(child)
    }
  }
  else {
    el.innerHTML = children
  }

  return el
}
