import h from './h.js'
import feelings from './feelings.js'
import colors from './colors.js'

const root = document.getElementById('app')

const GAP = 0 // horizontal
const LEVEL_GAP = 0

const RADII_0 = [60, 180]
const RADII_1 = [RADII_0[1] + LEVEL_GAP, 340]
const RADII_2 = [RADII_1[1] + LEVEL_GAP, 480]

function countFeelings (feelings) {
  let count = 0
  for (let feeling in feelings) {
    const childFeeling = feelings[feeling]
    if (childFeeling === true) count++
    else if (typeof childFeeling === 'object') {
      count += countFeelings(childFeeling)
    }
    else throw Error('unknown feeling ' + JSON.stringify(childFeeling))
  }

  return count
}

const totalBottomLevelFeelings = countFeelings(feelings)
const degreesPerBottomLevelFeeling = 360.0 / totalBottomLevelFeelings

function buildFeelingsData (feelings, degreesOffset = 0) {
  if (feelings === true) {
    return {
      angles: [degreesOffset, degreesOffset + degreesPerBottomLevelFeeling],
      children: []
    }
  }
  return Object.keys(feelings).reduce((acc, feeling, i, feelingsArr) => {
    const previousFeeling = (i === 0) ? null : acc[i - 1]
    const angleStart = previousFeeling ? previousFeeling.angles[1] : degreesOffset

    const feelingsBelow = countFeelings(feelings[feeling])
    const step = (feelingsBelow || 1) * degreesPerBottomLevelFeeling
    const angleEnd = angleStart + step

    acc.push({
      label: feeling,
      angles: [angleStart, angleEnd],
      children: buildFeelingsData(feelings[feeling], angleStart)
    })
    return acc
  }, [])
}

const feelingsData = buildFeelingsData(feelings)
// console.log(feelingsData)

const imageRad = RADII_2[1] + 50
const imageDia = imageRad * 2

root.appendChild(
  h('svg',
    { 
      viewBox: `-${imageRad} -${imageRad} ${imageDia} ${imageDia}`,
      width: imageDia,
      height: imageDia
    }, 

    feelingsData.map(({ label, angles, children }) => {
      const color = colors[label]
      return segment(
        {
          angles,
          gap: GAP,
          radii: RADII_0,
          label
        },
        {
          fill: color
        },

        children.map(({ label, angles, children }) => {
          return segment(
            {
              angles,
              gap: GAP,
              radii: RADII_1,
              label
            },
            {
              fill: color
            },
            
            children.map(({ label, angles }) => {
              return segment(
                {
                  angles,
                  gap: GAP,
                  radii: RADII_2,
                  label
                },
                {
                  fill: color
                },
              )
            })
          )
        })
      )
    })
  )
)

function segment ({ angles, gap, radii, label }, opts = {}, children = []) {
  const gapDegreesInner = gap ? (gap / radii[0] / Math.PI * 180) : 0
  const gapDegreesOuter = gap ? (gap / radii[1] / Math.PI * 180) : 0

  const midAngle = (angles[0] + angles[1]) / 2
  const midRadius = (radii[0] + radii[1]) / 2
  const center = polarToCartesian(0, 0, midRadius, midAngle)

  const feeling = h('g', { class: `${label} feeling` }, [
    h('path', {
      d: [
        describeArc(0, 0, radii[0], angles[0] + gapDegreesInner, angles[1] - gapDegreesInner),
        describeArc(0, 0, radii[1], angles[1] - gapDegreesOuter, angles[0] + gapDegreesOuter, 1)
          .replace('M', 'L'),

        'Z'
      ].join(' '),
      ...opts
    }),
    h('text', {
      fill: 'white',
      'text-anchor': 'middle',
      // dx: '-0.5em', // HACK: move it a bit more central
      dy: '0.3em', // HACK: centers text more
      transform: [
        `translate(${center.x}, ${center.y})`,
        `rotate(${midAngle < 180 ? midAngle - 90 : midAngle - 270})`,
      ].join(' ')
    }, label),
  ])

  feeling.addEventListener('click', handleFeelingClick)

  return h('g', { class: `${label} group` }, [
    feeling,
    ...children
  ])
}

function handleFeelingClick (ev) {
  let target = ev.target
  target.classList.contains('group')
  while (target && !target.classList.contains('group')) {
    target = target.parentElement
  }
  if (!target) return

  target.classList.toggle('active')
}

function describeArc (x, y, radius, startAngle, endAngle, sweepFlag = 0) {
  const start = polarToCartesian(x, y, radius, endAngle)
  const end = polarToCartesian(x, y, radius, startAngle)

  const largeArcFlag = (endAngle - startAngle) <= 180 ? 0 : 1

  return [
    "M", start.x, start.y,
    "A", radius, radius, 0, largeArcFlag, sweepFlag, end.x, end.y
  ].join(" ")
}

function polarToCartesian (centerX, centerY, radius, angleInDegrees) {
  const angleInRadians = (angleInDegrees - 90) * Math.PI / 180.0

  return {
    x: centerX + (radius * Math.cos(angleInRadians)),
    y: centerY + (radius * Math.sin(angleInRadians))
  }
}
